<?php


namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * PizzaPiz
 *
 * @ORM\Table(name="pizzaCommand")
 * @ORM\Entity
 */
class PizzaCommand
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Pizza
     *
     * @ORM\ManyToOne(targetEntity="Pizza")
     * @ORM\JoinColumn(name="pizza_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $pizza;

    /**
     * @var Size
     *
     * @ORM\ManyToOne(targetEntity="Size")
     */
    private $size;

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id):void
    {
        $this->id = $id;
    }

    /**
     * @return Pizza
     */
    public function getPizza():?Pizza
    {
        return $this->pizza;
    }

    /**
     * @param Pizza $pizza
     */
    public function setPizza(Pizza $pizza):void
    {
        $this->pizza = $pizza;
    }

    /**
     * @return Size
     */
    public function getSize():Size
    {
        return $this->size;
    }

    /**
     * @param Size $size
     */
    public function setSize(Size $size):void
    {
        $this->size = $size;
    }


}
<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientCli
 *
 * @ORM\Table(name="client")
 * @ORM\Entity
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var int
     *
     * @ORM\Column(name="postcode", type="integer", nullable=false)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=false)
     */
    private $phone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscriptionDate", type="datetime", nullable=false)
     */
    private $inscriptionDate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {

        $this->inscriptionDate = new \DateTime();
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName():?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName):void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName():?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName):void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getAddress():?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address):void
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getPostcode():?int
    {
        return $this->postcode;
    }

    /**
     * @param int $postcode
     */
    public function setPostcode(int $postcode):void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getCity():?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city):void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPhone():?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone):void
    {
        $this->phone = $phone;
    }

    /**
     * @return \DateTime
     */
    public function getInscriptionDate():\DateTime
    {
        return $this->inscriptionDate;
    }

    /**
     * @param \DateTime $inscriptionDate
     */
    public function setInscriptionDate(\DateTime $inscriptionDate):void
    {
        $this->inscriptionDate = $inscriptionDate;
    }


}

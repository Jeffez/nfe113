<?php


namespace App\Controller;


use App\Entity\Command;
use App\Entity\CommandStatus;
use App\Entity\DeliveryMan;
use App\Entity\Pizza;
use App\Form\DeliveryManFormType;
use App\Form\PizzaFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 */
class AdminController extends AbstractController
{

    /**
     * Homepage
     *
     * @Route("/admin", name="admin")
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @return Response
     */
    public function index(Request $request):Response
    {

        $id     = $request->query->get('id');
        $status = $request->query->get('status');

        $postId     = $request->request->get('id');
        $postStatus = $request->request->get('status');
        $deliveryMan = $request->request->get('delivery_man');

        if (null !== $id && null !== $status) {
            $cmd = $this->getDoctrine()->getRepository(Command::class)->find($id);
            if ($cmd instanceof Command) {
                $cmd->setStatus(
                    $this->getDoctrine()->getRepository(CommandStatus::class)->findOneBy(['name' => $status])
                );
            }
            $this->getDoctrine()->getManager()->persist($cmd);
            $this->getDoctrine()->getManager()->flush();
        }

        if (null !== $postId && null !== $postStatus && null !== $deliveryMan) {

            $cmd = $this->getDoctrine()->getRepository(Command::class)->find($postId);

            if ($cmd instanceof Command) {
                $cmd->setStatus(
                    $this->getDoctrine()->getRepository(CommandStatus::class)->findOneBy(['name' => $postStatus])
                );
                $cmd->setDeliveryMan(
                    $this->getDoctrine()->getRepository(DeliveryMan::class)->find($deliveryMan)
                );
            }
            dump($cmd);

            $this->getDoctrine()->getManager()->persist($cmd);
            $this->getDoctrine()->getManager()->flush();

        }



        $commands    = $this->getDoctrine()->getRepository(Command::class)->findAll();



        $deliveryMan = new DeliveryMan();

        $deliveryManForm = $this->createForm(DeliveryManFormType::class, $deliveryMan);
        $deliveryManForm->handleRequest($request);

        if ($deliveryManForm->isSubmitted() && $deliveryManForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($deliveryMan);
            $entityManager->flush();


        }


        $deliveryMen = $this->getDoctrine()->getRepository(DeliveryMan::class)->findAll();

        $pizza = new Pizza();

        $pizzaForm = $this->createForm(PizzaFormType::class, $pizza);
        $pizzaForm->handleRequest($request);

        if ($pizzaForm->isSubmitted() && $pizzaForm->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pizza);
            $entityManager->flush();

        }
        return $this->render(
            'admin.html.twig',
            [
                'commands'    => $commands,
                'deliveryMen' => $deliveryMen,
                'pizzaForm' => $pizzaForm->createView(),
                'deliveryManForm' => $deliveryManForm->createView()
            ]
        );
    }
}
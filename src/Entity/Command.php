<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CommandeCom
 *
 * @ORM\Table(name="command")
 * @ORM\Entity
 */
class Command
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var DeliveryMan
     *
     * @ORM\ManyToOne(targetEntity="DeliveryMan")
     */
    private $deliveryMan;

    /**
     * @var CommandStatus
     *
     * @ORM\ManyToOne(targetEntity="CommandStatus")
     */
    private $status;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client")
     */
    private $client;

    /**
     * @var PizzaCommand
     *
     * @ORM\ManyToMany(targetEntity="PizzaCommand")
     */
    private $pizzaList;

    public function __construct()
    {
        $this->pizzaList = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id):void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getComment():string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment):void
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getDate():\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date):void
    {
        $this->date = $date;
    }

    /**
     * @return DeliveryMan
     */
    public function getDeliveryMan():DeliveryMan
    {
        return $this->deliveryMan;
    }

    /**
     * @param DeliveryMan $deliveryMan
     */
    public function setDeliveryMan(DeliveryMan $deliveryMan):void
    {
        $this->deliveryMan = $deliveryMan;
    }

    /**
     * @return CommandStatus
     */
    public function getStatus():?CommandStatus
    {
        return $this->status;
    }

    /**
     * @param CommandStatus $status
     */
    public function setStatus(CommandStatus $status):void
    {
        $this->status = $status;
    }

    /**
     * @return Client
     */
    public function getClient():Client
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient(Client $client):void
    {
        $this->client = $client;
    }

    /**
     * @return PizzaCommand
     */
    public function getPizzaList()
    {
        return $this->pizzaList;
    }

    public function addPizzaList(PizzaCommand $pizzaList): self
    {
        if (!$this->pizzaList->contains($pizzaList)) {
            $this->pizzaList[] = $pizzaList;
        }

        return $this;
    }

    public function removePizzaList(PizzaCommand $pizzaList): self
    {
        if ($this->pizzaList->contains($pizzaList)) {
            $this->pizzaList->removeElement($pizzaList);
        }

        return $this;
    }



}

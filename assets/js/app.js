
// JavaScript Require
require('bootstrap/dist/js/bootstrap.min');
require('./pizza');
require('@fortawesome/fontawesome-free/js/all.js');

// Css Require
import '../css/app.css';
require('@fortawesome/fontawesome-free/css/all.min.css');
require('bootstrap/dist/css/bootstrap.min.css');


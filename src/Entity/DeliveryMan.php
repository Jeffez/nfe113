<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LivreurLivr
 *
 * @ORM\Table(name="deliveryMan")
 * @ORM\Entity
 */
class DeliveryMan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255, nullable=false)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=10, nullable=false)
     */
    private $phone;

    /**
     * @return string
     */
    public function getLastName():?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName):void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getFirstName():?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName):void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getPhone():?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone):void
    {
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getId():int
    {
        return $this->id;
    }


}

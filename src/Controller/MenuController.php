<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Command;
use App\Entity\CommandStatus;
use App\Entity\Pizza;
use App\Entity\PizzaCommand;
use App\Entity\Size;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 */
class MenuController extends AbstractController
{

    /**
     * Homepage
     *
     * @Route("/", name="menu")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function menu(Request $request):Response
    {
        $em = $this->getDoctrine()->getManager();
        $pizzaRepository = $em->getRepository(Pizza::class);
        $sizeRepository = $em->getRepository(Size::class);

        $pizzas = $pizzaRepository->findAll();
        $sizes  = $sizeRepository->findAll();

        $cmd =null;
        $idCmd = 0;

        if(!empty($request->request->get('deleteId'))){
            $idCommand = $request->request->get('deleteId');
            $idCmd = $request->request->get('idCmd');

            $pizzaCmd = $em->getRepository(PizzaCommand::class)->find($idCommand);

            $cmd = $em->getRepository(Command::class)->find($idCmd);

            $em->remove($pizzaCmd);
            $em->flush();
        }

        if (!empty($request->request->get('pizza'))) {


            $idCmd = $request->request->get('idCmd');
            $pizzaId = $request->request->get('pizza');
            $sizeId = $request->request->get('size');


            if($idCmd === '0'){
                $cmd = new Command();
                $cmd->setComment("");
                $cmd->setDate(new \DateTime());
                $cmd->setStatus($em->getRepository(CommandStatus::class)->find(1));
            }else {
                $cmd = $em->getRepository(Command::class)->find($idCmd);
            }

            $pizzaCmd = new PizzaCommand();
            $pizzaCmd->setPizza($pizzaRepository->find($pizzaId));
            $pizzaCmd->setSize($sizeRepository->find($sizeId));
            $em->persist($pizzaCmd);
            $cmd->addPizzaList($pizzaCmd);

            $em->persist($cmd);
            $em->flush();
            $idCmd = $cmd->getId();
        }

        return $this->render(
            'menu.html.twig', [
                'pizzas' => $pizzas,
                'sizes'  => $sizes,
                'idCmd'  => $idCmd,
                'commands' => $cmd,
            ]
        );
    }

    /**
     * Commande
     *
     * @Route("/submitCommand", name="submitCommand")
     *
     * @IsGranted("ROLE_USER")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function submitCommande(Request $request):Response
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $client = $em->getRepository(Client::class)->findOneBy(['user'=>$user]);

        if(null == $client){
            return $this->redirectToRoute('app_client');
        }

        $idCmd = $request->query->get('idCmd');

        if($idCmd !== '0'){
            $cmd = $em->getRepository(Command::class)->find($idCmd);
            $cmd->setStatus($em->getRepository(CommandStatus::class)->find(2));
            $cmd->setClient($client);
            $em->persist($cmd);
            $em->flush();

            return $this->render('traitement.html.twig');

        }

        return $this->redirectToRoute("menu",['idCmd'=>$idCmd]);
    }


    /**
     * Commande
     *
     * @Route("/delete", name="delete")
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function delete(Request $request):Response
    {

        $pizza = $this->getDoctrine()->getRepository(Pizza::class)->find($request->query->get('id'));

        $this->getDoctrine()->getManager()->remove($pizza);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute("menu");
    }
}
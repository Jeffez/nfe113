<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200630180253 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO commandStatus VALUES('1','CREATED')");
        $this->addSql("INSERT INTO commandStatus VALUES('2','PAID')");
        $this->addSql("INSERT INTO commandStatus VALUES('3','PREPARED')");
        $this->addSql("INSERT INTO commandStatus VALUES('4','DELIVERED')");
        $this->addSql("INSERT INTO commandStatus VALUES('5','CLOSE')");
        $this->addSql("INSERT INTO size VALUES('1', '0.00', 'SMALL')");
        $this->addSql("INSERT INTO size VALUES('2', '2.00', 'MEDIUM')");
        $this->addSql("INSERT INTO size VALUES('3', '3.50', 'LARGE')");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}

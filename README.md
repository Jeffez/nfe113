# Prerequisite 

You need to have installed on your linux :
* Composer
* Yarn
* PHP7.3
* Mysql

# Installation

1) Copy the file in your /var/www/ directory

2) Apply the following commands into the folder :
    ```
    composer install
    yarn install
    yarn run encore prod
    ```

3) Complete the .env file by replacing your MySQL's username and password

4) Run the following commands :
    ```
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:update --force
    php bin/console doctrine:migration:migrate
    ```
    
5) In your Database create a user with the roles : '["ROLE_ADMIN"]'

# Instruction unclear, I can not see the website

A live version is available at https://nfe113.jetruccharasch.fr/

The administrator logins are :

    ```
    username : admin@nfe113.fr
    password : admin@nfe113.fr
    ```